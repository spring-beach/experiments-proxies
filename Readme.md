# Learning Tests for Spring -Proxies

This project contains one set of experiments demonstrating how [AOP-proxies](#aop-proxies) behave in different Spring Boot versions. So far, all proxied beans have the scope singleton.

A [second set of tests](#emulating-proxies-with-decorators) explore whether the effects of CGLib-based AOP-proxies can be emulated using the decorator pattern. 


Note: this project contains tests that fail intentionally, see [Manual proxies](Readme.md#manual-proxies)

# AOP Proxies

## Questions and My Expectations

<dl>
<dt>What is the default proxying mechanism used in Spring Boot</dt>
<dd>JDK based proxies following standard documentation</dd>
<dt>How often is a constructor called?</dt>
<dd>Since Spring 4.0 it should be called once (Spring uses Objenesis for creating the proxies). CGLib proxies in older Spring versions called the constructor twice.</dd>
<dt>Is a collaborating component proxied?</dt>
<dd>Expectation: no</dd>
<dt>When a method returns a references to 'this' (with varying method signatures), does it return the proxy or the targeted object?</dt>
<dd>For CGLib I would expect the proxy, because proxying is done via subclasses. For simple interface-based proxies using delegation to the targeted object I would expect a reference to the plain target.</dd>
<dt>When a method returns a `ReferenceHolder` instance referencing 'this', is the proxy or the targeted object referenced?</dt>
<dd>Same answer..</dd>
<dt>When defining pointcuts on internal method calls, are they intercepted?</dt>
<dd>Documentation of, e.g., `@Transient`, indicates that this is not the case.</dd>
<dt>Does constructor injection work for the target objects?</dt>
<dd>For CGLib in Spring this was an issue in the past. Has been resolved in Spring 4.0 with the adoption of Objenesis.</dd>
<dt>Does the proxy use the hashCode of the target object or is another hashCode created?</dt>
<dd>? (using a separate hashCode for the proxy would mean that the proxying leaks at this point, but they are different objects)</dd>
<dt>Is the proxy equal to the target object?</dt>
<dd>? (should be consistent with hashCode); note: this experiments depends on the reference object</dd>
</dl>

Some of my expectatations refer to the following quote from the Spring Documentation:

>"CGLIB proxying works by generating a subclass of the target class at runtime. Spring configures this generated subclass to delegate method calls to the original target
>: the subclass is used to implement the Decorator pattern, weaving in the advice." 


## Results

> _Highlights_: Assuming that proxies should be transparent (i.e., the client does not know that it is dealing with a proxy), 
[+ these results +] could lead to inconsistencies, possible leaks of the target object, or indicate to a client that it is not the original target. 
> [- Red marks -] indicate differences between versions.


| **Question**                                                                                       | **Expectations**        | **JDK dynamic proxies**| **CGLib**|
|:---------------------------------------------------------------------------------------------------|:-------------------------|:-------------------|:------------|
| **Spring Boot 2.0.0.M3**                                                                           |                          |                    |             |
| Default Proxy Mechanism                                                                            | JDK                      |                    | [- default -]     |
| Constructor calls (count)                                                                          | 2 for CGLib              | 1                  | 1           |
| Constructor injection possible?                                                                    | yes                      | yes                | yes         |
| Stretch proxying to collaborating component                                                        | no                       | no                 | no          |
| Method returning 'this' (return type is the target type; method is final)         | CGLib: returns proxy<br/> JDK: returns plain target | JDK proxy    | CGLib proxy |
| Method returning 'this': return type is _Object_ 						      | CGLib: returns proxy<br/> JDK: returns plain target | [- CGLib proxy -]  | CGLib proxy |
| Method returning another object referencing 'this': <br/>`ReferenceHolder` contains a reference to plain target or to proxy | CGLib: contains proxy<br/> JDK: contains plain | [+ plain target +] |  [+ plain target +]|
| Interception of pointcuts in internal method calls                                                 | documentation: no        |  no                |  no         |
| hashCode of proxy equal to hashCode of target object											     | ?				        | [+ no +]           | [+ no +]    |
| proxy equals target object                        											     | ?				        | [+ no +]           | [+ no +]    |
| **Spring Boot 1.5.6.RELEASE**                                                                      |                          |                    |             |
| Default Proxy Mechanism                                                                            | JDK                      | [- default -]            |             |
| Constructor calls (count)                                                                          | 2 for CGLib              | 1                  | 1           |
| Constructor injection possible?                                                                    | yes                      | yes                | yes         |
| Stretch proxying to collaborating component                                                        | no                       | no                 | no          |
| Method returning 'this' (return type is the target type; method is final)         | CGLib: returns proxy<br/> JDK: returns plain target | JDK proxy    | CGLib proxy |
| Method returning 'this': return type is _Object_ 						     | CGLib: returns proxy<br/> JDK: returns plain target | [- plain target -]  | CGLib proxy |
| Method returning another object referencing 'this': <br\>`ReferenceHolder` contains a reference to plain target or to proxy | CGLib: contains proxy<br/> JDK: contains plain | plain target  |  plain target |
| Interception of pointcuts in internal method calls                                                 | documentation: no        |  no                |  no         |
| hashCode of proxy equal to hashCode of target object											     | ?				        |  no                |  no         |
| proxy equals target object                        										         | ?				        |  no                |  no         |

## How it works

### Test classes
The experiment uses three test classes (in the package `o.t.b.examples.proxy`) with the following scope:

* `DefaultProxyExperiments` shows which proxy mechanism is used by default
* `CGLibProxyExperiments` all other tests for CGLib-based proxies of a singleton bean
* `JDKProxyExperiments` all other tests for dynamic proxies of a singleton bean

Test method names should indicate the results for the main Spring Boot version (2.0.0.M3).

> To understand the behavior in different Spring Boot versions, change the version in the POM file.

### Proxies

All proxied Spring beans are declared in `ProxyConfig`. They are services of type `ExperimentalTargetImpl` implementing the interface `ExperimentalTarget`. Pointcuts in the services are declared by using the annotation `@Weave` on a method.

Using Spring's AOP support, the proxies are created by applying an aspect (`AnnotationBasedAspect`) on these services.
More precisely, the `AnnotationBasedAspect` intercepts calls to methods annotated with `@Weave`. 

### Exploring "this"

Some of the experiments what `this` references. Variations for returning `this` include

* method return type is `Object`
* method return type is `ExperimentalTarget`
* method returns a `ReferenceHolder` instance that in turn has an attribute (declared as `Object`) referencing the target object
* final method; return type is `ExperimentalTarget`
 
# Emulating proxies with decorators

The package ..proxies.manual contains test for proxies that use subtypes and delegation to create plain proxies that are not managed by Spring.
This was a (failed) attempt to recreate CGLib effects without bytecode manipulations, based on the
[Spring documentation](https://docs.spring.io/spring/docs/4.3.12.RELEASE/spring-framework-reference/htmlsingle/#aop-api-proxying-class): 

>"CGLIB proxying works by generating a subclass of the target class at runtime. Spring configures this generated subclass to delegate method calls to the original target
>: the subclass is used to implement the Decorator pattern, weaving in the advice." 

None of the plain proxies show the same behavior as a CGLib proxy though (indicated through the [-failed-] unit tests). 

## Next Steps

* Internal method call to a pointcut starting in a method that is not intercepted: is the internal method call intercepted? Expectation: no (it is still worth testing because it could be the source of some nasty bugs).
* Is the behavior the same for other bean scopes?
* Replace green color in table with something else (that does not imply that things are ok)
* Expand the section on emulating proxies w decorators
    * Compare CGLib with decorator patterns for `DecoratingProxy` and `SubclassingProxy`: where does it fail?
