package org.tbuttler.examples.matches;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.junit.Assert.assertThat;

/**
 * Checks the type of proxy that is created or returned by Spring. This implementation
 * uses classnames to identify the proxy type.
 * 
 * @author Tanja
 *
 */
public class ProxyChecks {

	public static final String COM_SUN_PROXY = "com.sun.proxy";
	public static final String SPRING_CGLIB = "SpringCGLIB";

	/**
	 * Checks whether this is the proxy created by the JDK.
	 * 
	 * @param proxy
	 */
	public static void isInterfaceProxy(Object proxy) {
		classNameContains(COM_SUN_PROXY, proxy);
	}

	public static void isCGLIBProxy(Object proxy) {
		classNameContains(SPRING_CGLIB, proxy);
	}
	
	public static void classNameContains(String substring, Object investigated) {
		String className = investigated.getClass().getName();
		assertThat(className, containsString(substring));
	}

	public static void isNoProxy(Object noProxy) {
		String className = noProxy.getClass().getName();
		assertThat(className, not(startsWith(COM_SUN_PROXY)));
		assertThat(className, not(containsString(SPRING_CGLIB)));
	}

	/**
	 * Different proxy types can be identified by different class names. This
	 * test is more finegrained than testing simply for interface based or CGLib
	 * proxies, as it takes the subtype generated via reflection into account.
	 * 
	 * <br/>
	 *  Use this method to check if the same
	 * dynamically generated type is used in two proxies.
	 * 
	 * @param proxy
	 * @param comparison
	 */
	public static void sameClassNames(Object proxy, Object comparison) {
		assertThat(proxy.getClass().getName(), is(equalToIgnoringCase(comparison.getClass().getName())));
	}

	/**
	 * Different proxy types can be identified by different class names. 
	 * This
	 * test is more finegrained than testing simply for interface based or CGLib
	 * proxies, as it takes the subtype generated via reflection into account.
	 * <br/>
	 * Use
	 * this method to check if the different dynamically generated type is used
	 * in two proxies.
	 */
	public static void differentClassNames(Object proxy, Object comparison) {
		assertThat(proxy.getClass().getName(), not(equalToIgnoringCase(comparison.getClass().getName())));
	}

}
