package org.tbuttler.examples.matches;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.tbuttler.examples.aspect.AnnotationBasedAspect;

/**
 * Checks how often the advice was executed. Use these assertions for easy readability.
 * 
 * @author Tanja
 *
 */
public class AspectChecks {
	
	public static void adviceExecutedOnce(AnnotationBasedAspect aspect) {
		assertThat(aspect.getObservations(), is(1));
	}

	public static void adviceExecutedTwice(AnnotationBasedAspect aspect) {
		assertThat(aspect.getObservations(), is(2));
	}
	
	public static void adviceNotExecuted(AnnotationBasedAspect aspect) {
		assertThat(aspect.getObservations(), is(0));
	}
}
