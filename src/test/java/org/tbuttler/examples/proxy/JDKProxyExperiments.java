package org.tbuttler.examples.proxy;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.junit.Assert.assertThat;
import static org.tbuttler.examples.matches.AspectChecks.adviceExecutedOnce;
import static org.tbuttler.examples.matches.AspectChecks.adviceNotExecuted;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.tbuttler.examples.aspect.AnnotationBasedAspect;
import org.tbuttler.examples.aspect.targets.ExperimentalTarget;
import org.tbuttler.examples.helper.DummyComponent;
import org.tbuttler.examples.matches.ProxyChecks;

/**
 * Answers the experimental questions ({@link ExperimentalTarget}) for the
 * JDK proxy (also called interface-based proxy). Method names indicate the results for Spring Boot version
 * 2.0.0.M3.
 * 
 * @author Tanja
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
// @DirtiesContext(classMode=ClassMode.BEFORE_EACH_TEST_METHOD)
public class JDKProxyExperiments {

	@Autowired
	@Qualifier("jdkTarget")
	private ExperimentalTarget jdkTarget;

	@Autowired
	private AnnotationBasedAspect aspect;

	@Test
	@DirtiesContext
	public void constructorIsCalledOnce() {
		assertThat(jdkTarget.getConstructorCalls(), is(1));
		// getConstructorCalls was called once
		adviceExecutedOnce(aspect);
	}

	@Test
	public void testedComponentIsJDKProxy() {
		ProxyChecks.isInterfaceProxy(jdkTarget);
		adviceNotExecuted(aspect);
	}

	@Test
	public void allowsConstructorInjection() {
		assertThat(jdkTarget.getCore().getLabel(), is(equalToIgnoringCase("dummy")));
	}

	@Test
	public void injectedDummyComponentIsNoProxy() {
		ProxyChecks.sameClassNames(jdkTarget.getCore(), new DummyComponent());
	}

	@Test
	public void selfReturnsProxy() {
		ProxyChecks.isInterfaceProxy(jdkTarget.getSelf());
		assertThat(jdkTarget.getSelf(), is(jdkTarget));
		ProxyChecks.sameClassNames(jdkTarget.getSelf(), jdkTarget);
		adviceNotExecuted(aspect);
	}

	@Test
	public void referenceHolderContainsPlainTarget() {
		ProxyChecks.isNoProxy(jdkTarget.getReferenceToSelf().getReference());
		ProxyChecks.differentClassNames(jdkTarget.getReferenceToSelf().getReference(), jdkTarget);
		adviceNotExecuted(aspect);
	}

	/**
	 * For Spring 4.3, the plain target is returned.
	 */
	@Test
	public void selfAsObjectReturnsCGLIBProxyInSpring5() {
		// CGLib proxy is a real surprise!
		ProxyChecks.isCGLIBProxy(jdkTarget.getSelfAsObject());
		// NOTE it is a different proxy type, but still passes equals and
		// hashCode test
		assertThat(jdkTarget.getSelfAsObject(), is(jdkTarget));
		assertThat(jdkTarget.getSelfAsObject().hashCode(), is(jdkTarget.getSelfAsObject().hashCode()));
		ProxyChecks.differentClassNames(jdkTarget.getSelfAsObject(), jdkTarget);
		adviceNotExecuted(aspect);
	}

	@Test
	public void finalSelfReturnsProxy() {
		ProxyChecks.isInterfaceProxy(jdkTarget.getSelfFromFinalMethod());
		assertThat(jdkTarget.getSelfFromFinalMethod(), is(jdkTarget));
		ProxyChecks.sameClassNames(jdkTarget.getSelfFromFinalMethod(), jdkTarget);
		adviceNotExecuted(aspect);
	}

	@Test
	public void cannotOverrideHashCodeSurprise() {
		ExperimentalTarget plainTarget = (ExperimentalTarget) jdkTarget.getReferenceToSelf().getReference();
		assertThat(jdkTarget.hashCode(), is(not(1)));
		assertThat(jdkTarget.hashCode(), is(not(plainTarget.hashCode())));
		adviceNotExecuted(aspect);
	}
	
	@Test
	public void plainTargetDoesNotEqualProxy() {
		ExperimentalTarget plainTarget = (ExperimentalTarget) jdkTarget.getReferenceToSelf().getReference();
		assertThat(jdkTarget, not(equalTo(plainTarget)));
		adviceNotExecuted(aspect);
	}

	@Test
	@DirtiesContext
	public void doesNotInterceptInternalMethodCall() {
		jdkTarget.internalMethodCall();
		adviceExecutedOnce(aspect);
	}
}
