package org.tbuttler.examples.proxy;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.junit.Assert.assertThat;
import static org.tbuttler.examples.matches.AspectChecks.adviceExecutedOnce;
import static org.tbuttler.examples.matches.AspectChecks.adviceNotExecuted;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.tbuttler.examples.aspect.AnnotationBasedAspect;
import org.tbuttler.examples.aspect.targets.ExperimentalTarget;
import org.tbuttler.examples.matches.ProxyChecks;

/**
 * Answers the experimental questions ({@link ExperimentalTarget}) for the CGLib proxy. Method names indicate the results
 * for Spring Boot version 2.0.0.M3. 
 *  
 * @author Tanja
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
//@DirtiesContext(classMode=ClassMode.BEFORE_EACH_TEST_METHOD)
public class CGLibProxyExperiments {
	
	@Autowired
	@Qualifier("cglibTarget")
	private ExperimentalTarget cglibProxy;
	
	@Autowired
	private AnnotationBasedAspect aspect;
	
	@Test
	@DirtiesContext
	public void constructorIsCalledOnce() {
		assertThat(cglibProxy.getConstructorCalls(), is(1));
		adviceExecutedOnce(aspect);
	}
	
	@Test
	public void testedComponentIsCGLIBProxy(){
		ProxyChecks.isCGLIBProxy(cglibProxy);
		//System.err.println(cglibProxy.getClass());
		adviceNotExecuted(aspect);
	}

	@Test
	public void allowsConstructorInjection() {
		assertThat(cglibProxy.getCore().getLabel(), is(equalToIgnoringCase("dummy")));
		adviceNotExecuted(aspect);
	}
	
	@Test
	@DirtiesContext
	public void doesNotinterceptInternalMethodCalls() {
		cglibProxy.internalMethodCall();
		adviceExecutedOnce(aspect);
	}
	
	
	@Test
	public void selfReturnsProxy() {
		ProxyChecks.isCGLIBProxy(cglibProxy.getSelf());
		//transparent self reference
		assertThat(cglibProxy.getSelf(), is(cglibProxy));
		ProxyChecks.sameClassNames(cglibProxy.getSelf(), cglibProxy);
		adviceNotExecuted(aspect);
	}
	
	@Test
	public void finalSelfReturnsProxy() {
		ProxyChecks.isCGLIBProxy(cglibProxy.getSelf());
		//transparent self reference
		assertThat(cglibProxy.getSelf(), is(cglibProxy));
		ProxyChecks.sameClassNames(cglibProxy.getSelf(), cglibProxy);
		adviceNotExecuted(aspect);
	}
	
	@Test
	public void referenceHolderContainsPlainTarget() {
		Object innerReference = cglibProxy.getReferenceToSelf().getReference();
		//brittle self reference
		ProxyChecks.isNoProxy(innerReference);
		ProxyChecks.differentClassNames(innerReference, cglibProxy);
		adviceNotExecuted(aspect);
	}
	
	@Test
	public void selfAsObjectReturnsProxy() {
		ProxyChecks.isCGLIBProxy(cglibProxy.getSelfAsObject());
		//transparent self reference
		assertThat(cglibProxy.getSelfAsObject(), is(cglibProxy));
		ProxyChecks.sameClassNames(cglibProxy.getSelfAsObject(), cglibProxy);
		adviceNotExecuted(aspect);
	}

	@Test
	public void proxyHasDifferentHashCodeThanTarget() {
		assertThat(cglibProxy.hashCode(), is(not(1)));
		adviceNotExecuted(aspect);
	}
	
	@Test
	public void plainTargetDoesNotEqualProxy() {
		ExperimentalTarget plainTarget = (ExperimentalTarget) cglibProxy.getReferenceToSelf().getReference();
		assertThat(cglibProxy, not(equalTo(plainTarget)));
		adviceNotExecuted(aspect);
	}

}
