package org.tbuttler.examples.proxy;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.tbuttler.examples.aspect.targets.ExperimentalTarget;
import org.tbuttler.examples.matches.ProxyChecks;

/**
 * Checks what the default proxy type. Method names indicate the results for
 * Spring Boot version 2.0.0.M3. <br/>
 * Spring Boot 1.5.6.RELEASE uses a JDK proxy, Spring Boot 2.0.0.M3 uses a CGLib
 * proxy.
 * 
 * @author Tanja
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class DefaultProxyTypeExperiments {

	@Autowired
	@Qualifier("defaultProxyType")
	private ExperimentalTarget defaultProxy;

	@Autowired
	@Qualifier("cglibTarget")
	private ExperimentalTarget cglibProxy;

	@Test
	public void isCGLib() {
		ProxyChecks.isCGLIBProxy(defaultProxy);
		// System.err.println(defaultProxy.getClass());
		// System.err.println(cglibProxy.getClass());
	}

	/**
	 * 
	 * Both CGLib-based proxies actually differ in their class names, i.e., they
	 * are different subtypes of the CGLib enhanced proxy version (even though
	 * they use the same configuration and the same aspect definition).
	 */
	@Test
	public void notSameSubtypeAsCGLibProxy() {
		//TODO what does this mean for the advice? Are they acting independently of each other (no side-effects)?
		ProxyChecks.differentClassNames(defaultProxy, cglibProxy);
	}

}
