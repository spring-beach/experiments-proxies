package org.tbuttler.examples;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.tbuttler.examples.aspect.targets.ExperimentalTarget;
import org.tbuttler.examples.aspect.targets.ExperimentalTargetImpl;
import org.tbuttler.examples.helper.DummyComponent;

/**
 * Creates proxies of {@link ExperimentalTarget} for the experiments.
 * 
 * @author Tanja
 *
 */
@Configuration
public class ProxyConfig {
	
	/**
	 * Proxy using the JDK proxy implementation.
	 * 
	 * @param component dummy collaborator
	 * @return
	 */
	@Bean("jdkTarget")
	@Scope(proxyMode=ScopedProxyMode.INTERFACES)
	public ExperimentalTarget getJDKProxiedParentBean(DummyComponent component) {
		return new ExperimentalTargetImpl(component);
	}
	
	/**
	 * Proxy using the CGLib proxying mechanism.
	 * 
	 * @param component dummy collaborator
	 * @return
	 */
	@Bean("cglibTarget")
	@Scope(proxyMode=ScopedProxyMode.TARGET_CLASS)
	public ExperimentalTarget getCGLIBProxiedTarget(DummyComponent component) {
		return new ExperimentalTargetImpl(component);
	}
	
	/**
	 * Proxy type is not prescribed: returns whatever the default proxy type is in a Spring Boot version.
	 * @param component dummy collaborator
	 * @return
	 */
	@Bean("defaultProxyType")
	public ExperimentalTarget getDefaultProxiedTarget(DummyComponent component) {
		return new ExperimentalTargetImpl(component);
	}
	
//	@Bean("frozenCglibTarget")
//	public FakeTarget getFrozenCGLIBProxiedTarget(DummyComponent component) {
//		ProxyFactory factory = new ProxyFactory();
//		factory.addAdvice(new AnnotationBasedAspect());
//		factory.setTarget(new FakeTargetImpl(component));
//		factory.setFrozen(true);
//		factory.setProxyTargetClass(true);
//		return (FakeTarget) factory.getProxy();
//	}
//	

}
