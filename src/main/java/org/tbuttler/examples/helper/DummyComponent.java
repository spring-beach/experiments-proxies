package org.tbuttler.examples.helper;

import org.springframework.stereotype.Component;

/**
 * Dummy component. Used to test whether proxies have any effect on composition
 * (expected: none) and constructor injection (expected: none for current
 * versions of Spring (had some effect on JDKProxies in the past)).
 * 
 * @author Tanja
 *
 */
@Component
public class DummyComponent {

	public String getLabel() {
		return "dummy";
	}
}
