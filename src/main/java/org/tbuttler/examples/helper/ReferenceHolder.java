package org.tbuttler.examples.helper;

public class ReferenceHolder {
	
	private Object reference;

	public ReferenceHolder(Object reference) {
		setReference(reference);
	}

	public Object getReference() {
		return reference;
	}

	public void setReference(Object reference) {
		this.reference = reference;
	}

}
