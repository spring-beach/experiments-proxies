package org.tbuttler.examples.proxies.manual;

import org.tbuttler.examples.aspect.AnnotationBasedAspect;
import org.tbuttler.examples.aspect.targets.ExperimentalTargetImpl;
import org.tbuttler.examples.helper.DummyComponent;

/**
* Proxy implementation relying on simple subclassing: SubclassingProxy is a subclass
 * of the target class that delegates to super, applying the given aspect if applicable.
 * <p>
* Spring documentation: CGLIB proxying works by generating a subclass of the target class at runtime (here: SubclassingProxy).
* Spring configures this generated subclass (here: SubclassingProxy) to delegate method calls to the
* original target (here: using super): the subclass is used to implement the Decorator pattern,
* weaving in the advice.
* 
* @see https://docs.spring.io/spring/docs/current/spring-framework-reference/htmlsingle/#aop-api-proxying-class
* @author Tanja
*
*/
public class SubclassingProxy extends ExperimentalTargetImpl {
	
	private AnnotationBasedAspect aspect;

	public SubclassingProxy(DummyComponent core, AnnotationBasedAspect aspect) {
		super(core);
		this.aspect = aspect;
	}

	@Override
	public int getConstructorCalls() {
		aspect.observeInjection();
		return super.getConstructorCalls();
	}

	@Override
	public void internalMethodCall() {
		aspect.observeInjection();
		super.internalMethodCall();
	}
	
	
}
