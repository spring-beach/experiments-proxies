package org.tbuttler.examples.proxies.manual;

import org.tbuttler.examples.aspect.AnnotationBasedAspect;
import org.tbuttler.examples.aspect.targets.ExperimentalTarget;
import org.tbuttler.examples.helper.DummyComponent;
import org.tbuttler.examples.helper.ReferenceHolder;

/**
* Is a decorating proxy for an ExperimentalTarget. Implements the interface {@link ExperimentalTarget}, and
* delegates method calls to the target instance and the aspect (if applicable).
* <p> 
* This proxy implementation that tries to mimic the documented CGLib
* mechanism: 
* CGLIB proxying works by generating a subclass of the target class at runtime.
* Spring configures this generated subclass to delegate method calls to the
* original target: the subclass is used to implement the Decorator pattern,
* weaving in the advice.
* 
* @see https://docs.spring.io/spring/docs/current/spring-framework-reference/htmlsingle/#aop-api-proxying-class
* @author Tanja
*
*/
public class DecoratingProxy implements ExperimentalTarget {
	
	private ExperimentalTarget decoratedTarget;
	private AnnotationBasedAspect aspect;
	
	public DecoratingProxy(ExperimentalTarget decoratedTarget, AnnotationBasedAspect aspect) {
		this.decoratedTarget = decoratedTarget;
		this.aspect = aspect;
	}

	public int getConstructorCalls() {
		aspect.observeInjection();
		return decoratedTarget.getConstructorCalls();
	}

	public DummyComponent getCore() {
		return decoratedTarget.getCore();
	}

	public ExperimentalTarget getSelf() {
		return decoratedTarget.getSelf();
	}

	public Object getSelfAsObject() {
		return decoratedTarget.getSelfAsObject();
	}

	public ReferenceHolder getReferenceToSelf() {
		return decoratedTarget.getReferenceToSelf();
	}

	public void internalMethodCall() {
		aspect.observeInjection();
		decoratedTarget.internalMethodCall();
	}

	public ExperimentalTarget getSelfFromFinalMethod() {
		return decoratedTarget.getSelfFromFinalMethod();
	}

}
