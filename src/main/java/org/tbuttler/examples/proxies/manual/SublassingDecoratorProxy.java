package org.tbuttler.examples.proxies.manual;

import org.tbuttler.examples.aspect.AnnotationBasedAspect;
import org.tbuttler.examples.aspect.targets.ExperimentalTarget;
import org.tbuttler.examples.aspect.targets.ExperimentalTargetImpl;
import org.tbuttler.examples.helper.DummyComponent;
import org.tbuttler.examples.helper.ReferenceHolder;

/**
 * Create a 'shell' of an {@link ExperimentalTargetImpl}, inject another
 * instance of the ExperimentalTargetImpl, and use the decorator pattern to apply aspects.
 * 
 * @author Tanja
 *
 */
public class SublassingDecoratorProxy extends ExperimentalTargetImpl {

	private ExperimentalTargetImpl target;

	private AnnotationBasedAspect aspect;

	public SublassingDecoratorProxy(DummyComponent core, ExperimentalTargetImpl target, AnnotationBasedAspect aspect) {
		super(core);
		this.target = target;
		this.aspect = aspect;
	}

	public boolean equals(Object obj) {
		return target.equals(obj);
	}

	public int getConstructorCalls() {
		aspect.observeInjection();
		return target.getConstructorCalls();
	}

	public DummyComponent getCore() {
		return target.getCore();
	}

	public ExperimentalTarget getSelf() {
		return target.getSelf();
	}

	public ReferenceHolder getReferenceToSelf() {
		return target.getReferenceToSelf();
	}

	public int hashCode() {
		return target.hashCode();
	}

	public Object getSelfAsObject() {
		return target.getSelfAsObject();
	}

	public void internalMethodCall() {
		aspect.observeInjection();
		target.internalMethodCall();
	}

	public String toString() {
		return target.toString();
	}

}
