package org.tbuttler.examples.aspect;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Use to declare pointcuts on methods.
 * 
 * @author Tanja
 *
 */
@Retention(RUNTIME)
@Target(METHOD)
public @interface Weave {

}
