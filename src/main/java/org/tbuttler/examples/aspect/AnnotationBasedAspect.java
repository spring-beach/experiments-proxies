package org.tbuttler.examples.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * Spy aspect used for triggering the creation of proxies. The aspects counts
 * how many times it observed a call to a method annotated with {@code @Weave}.
 * 
 * @author Tanja
 *
 */
@Aspect
@Component
public class AnnotationBasedAspect {

	/**
	 * Pointcut: any method annotated with {@code @Weave} is advised.
	 */
	private static final String pointcut = "@annotation(Weave)";

	private int observations;

	public AnnotationBasedAspect() {
		observations = 0;
	}

	/**
	 * Advice. Registers another observation.
	 */
	@Before(pointcut)
	public void observeInjection() {
		addObservation();
	}

	/**
	 * Returns the number of observations.
	 * 
	 * @return number of observations
	 */
	public int getObservations() {
		return observations;
	}

	private void addObservation() {
		observations++;
	}

	/**
	 * Sets the number of observations back to 0.
	 */
	public void resetObservations() {
		observations = 0;
	}

}
