package org.tbuttler.examples.aspect.targets;

import org.tbuttler.examples.aspect.Weave;
import org.tbuttler.examples.helper.DummyComponent;
import org.tbuttler.examples.helper.ReferenceHolder;

/**
 * Fake target interface following the typical declaration of services in
 * Spring. Implementations are the actual target for advice. They should use the
 * annotation {@link Weave} to indicate pointcuts.
 * 
 * <br/>
 * Declares methods to answer the following questions:
 * 
 * <dl>
 * <dt>How often is a constructor called?</dt>
 * <dd>Clean implementations should call it once, CGLib proxies had a bug that called
 * the constructor twice.</dd>
 * <dt>Is a collaborating component proxied?</dt>
 * <dd>Expectation: no</dd>
 * <dt>When a method returns a references to 'this', does it return the proxy or the
 * targeted object?</dt>
 * <dd>For CGLib I would expect the proxy, because proxying is done via
 * subclasses. For simple interface-based proxies using delegation to the
 * targeted object I would expect the target.</dd>
 * <dt>When defining pointcuts on internal method calls, are they intercepted?</dt>
 * <dd>Documentation of, e.g., @Transient, indicates that this is not the case.</dd>
 * <dt>Does constructor injection work for the target objects?</dt>
 * <dd>For CGLib in Spring this was an issue in the past. Should be resolved by now.</dd>
 * </dl>
 * 
 * 
 * @author Tanja
 *
 */
public interface ExperimentalTarget {

	/**
	 * Subclasses should count how often a constructor was called, and return
	 * the result with this method.
	 * 
	 * @return the number of times the constructor was called
	 */
	// @Weave - does not work on the interface - not inherited
	// does work on the abstract implementations!
	int getConstructorCalls();

	/**
	 * Subclasses should have a DummyComponent injected, and return it with this
	 * reference.
	 * 
	 * @return an injected component
	 */
	DummyComponent getCore();

	/**
	 * Subclasses should return a self-reference (this). NOTE: return type
	 * differs from {@link ExperimentalTarget#getSelfAsObject()}.
	 * 
	 * @return this instance
	 */
	ExperimentalTarget getSelf();

	/**
	 * Subclasses should return a self-reference (this). NOTE: return type
	 * differs from {@link ExperimentalTarget#getSelf()}.
	 * 
	 * @return this instance
	 */
	Object getSelfAsObject();

	/**
	 * Subclasses should place a self-reference into a ReferenceHolder, and
	 * return the reference holder.
	 * 
	 * @return an object containing a reference to this instance
	 */
	ReferenceHolder getReferenceToSelf();

	/**
	 * Should trigger an internal method call to another intercepted method.
	 */
	void internalMethodCall();

	/**
	 * Subclasses should make this method final (no overwriting), and return a
	 * self-reference (this).
	 * 
	 * @return this instance
	 */
	ExperimentalTarget getSelfFromFinalMethod();

}
