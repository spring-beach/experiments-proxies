package org.tbuttler.examples.aspect.targets;

import org.tbuttler.examples.aspect.Weave;
import org.tbuttler.examples.helper.DummyComponent;
import org.tbuttler.examples.helper.ReferenceHolder;


public class ExperimentalTargetImpl implements ExperimentalTarget {

	private int constructorCalls;
	private DummyComponent core;
	
	public ExperimentalTargetImpl(DummyComponent core) {
		constructorCalls++;
		this.core = core;
	}

	/**
	 * First pointcut for the aspect. Simple getter method that does not call any other pointcuts.
	 */
	@Weave
	@Override
	public int getConstructorCalls() {
		return constructorCalls;
	}

	@Override
	public DummyComponent getCore() {
		return core;
	}
	
	@Override
	public ExperimentalTarget getSelf() {
		return this;
	}
	
	@Override
	public ReferenceHolder getReferenceToSelf() {
		return new ReferenceHolder(this);
	}
	
	@Override
	public int hashCode() {
		System.err.println("get hash code");
		return 1;
	}

	@Override
	public Object getSelfAsObject() {
		return this;
	}
	
	/**
	 * Second pointcut. Internally calls the first pointcut method.
	 */
	@Override
	@Weave
	public void internalMethodCall() {
		System.out.println(getConstructorCalls());
	}

	@Override
	public final ExperimentalTarget getSelfFromFinalMethod() {
		return this;
	}
}
